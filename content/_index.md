---
title: SmallYTChannel Discord Bot
subtitle:  The bot runs on the SmallYTChannel Discord channel. The bot is written in Python and Javascript
comments: false
---

### Preconfigured bot prefix - ``.``
### Preconfigured dumpyard - ``mod-log``

Supported commands:

- ``kick @[user-name]`` - This kicks the user specified in ``user-name``. A success message is sent where the kick message was sent as well as in the dump yard.
- ``ban @[user-name]`` - This bans the user specified in ``user-name``. A success message is sent where the ban message was sent as well as in the dump yard.
- ``ping`` - Replies Pong. This command is to check whether the bot is active and properly functioning.
- ``createrolemessage`` - Creates a message that will give roles based on reactions. This part of the bot was derived from [Sam-DevZ's Discord-RoleReact](https://github.com/Sam-DevZ/Discord-RoleReact)

Other features:

- Kick message: A message is sent in the dumpyard specifying the user that has been kicked.
- Ban message: A message is sent in the dumpyard specifying the user that has been banned.
- Edited message: A message is sent in the dumpyard containing the original message as well as the new edited message.
- Deleted message: A message is sent containing the deleted message.
- Slow-mode: A specified channel is placed in slow mode. Users can only send one message for a specified amount of time. Moderators are immune (if they can kick members, the bot assumes that the  user is a moderator)